﻿using UnityEngine;
using System.Collections;

public class gameControls : MonoBehaviour {

	private Vector3 spawnPosition;
	private float 	startCoordinate,
					endCoordinate; 
	private bool pendingBlock;
	private GameObject currentToy;

	private bool repositionCamera;
	private float newCameraCoordinate;

	public GameObject toy;
	public GameObject levelBase;
	private Rigidbody toyBody;

	// Use this for initialization
	void Start () {
		startCoordinate = levelBase.transform.position.x - levelBase.GetComponent<Renderer>().bounds.size.x / 2;
		endCoordinate   = levelBase.transform.position.x + levelBase.GetComponent<Renderer>().bounds.size.x / 2;
		spawnPosition   = new Vector3(0,0,0);
		spawnPosition.x = startCoordinate;
		pendingBlock    = false;
		repositionCamera= false;
		newCameraCoordinate = Camera.main.transform.position.y;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonUp(0)){
			if (pendingBlock == false){
				spawnPosition.y = Camera.main.transform.position.y;
				currentToy = Instantiate( toy, spawnPosition, Quaternion.identity ) as GameObject;
				movementLoop toyBehavior = currentToy.GetComponent<movementLoop>();

				toyBehavior.startCoordinate = startCoordinate;
				toyBehavior.endCoordinate   = endCoordinate;

				toyBody = currentToy.GetComponent<Rigidbody>();
				pendingBlock = true;
			} else {
				if( toyBody != null ) {
					movementLoop toyBehavior = currentToy.GetComponent<movementLoop>();
					toyBehavior.moving = false;
					toyBody.useGravity = true;
					Debug.Log(  currentToy.GetComponent<Renderer>().bounds.size.y / 2 ); 
					newCameraCoordinate += currentToy.GetComponent<Renderer>().bounds.size.y / 2;
					pendingBlock = false;
				}
			}
		}

		if( repositionCamera ) {
			if( Camera.main.transform.position.y >= newCameraCoordinate ){
				repositionCamera = false;
			} else {
				Camera.main.transform.position = new Vector3(
					Camera.main.transform.position.x,
					Camera.main.transform.position.y + 2 * Time.deltaTime,
					Camera.main.transform.position.z
				);
			}
		}
	}
}
