﻿using UnityEngine;
using System.Collections;

public class DespawnHeight : MonoBehaviour {

	public float despawnCoordinate = 0;

	// Update is called once per frame
	void FixedUpdate () {
		if( transform.position.y <= despawnCoordinate ){
			Destroy( gameObject );
		}		
	}
}
