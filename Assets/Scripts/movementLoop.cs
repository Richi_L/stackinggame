﻿using UnityEngine;
using System.Collections;

public class movementLoop : MonoBehaviour {

	public float startCoordinate = 0;
	public float endCoordinate   = 0;
	public float speed			 = 5;
	public bool  moving = true;

	private int	 direction		 = 1;

	// Update is called once per frame
	void Update () {
		float destination;
		if( direction > 0 ){
			destination = endCoordinate;
		} else {
			destination = startCoordinate;
		}
			
		if( direction > 0 && transform.position.x >= destination 
		||  direction < 0 && transform.position.x <= destination ){
			direction *= -1;
		}

		if( moving ) {
			transform.position = new Vector3( 
				transform.position.x + direction * speed * Time.deltaTime, 
				transform.position.y, 
				transform.position.z 
			);
		}
	}
}
